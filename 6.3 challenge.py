# 6.3 Challenge: Convert Temperatures

def convert_cel_to_far(temp_c):
    temp_f = float(temp_c) * 9/5 + 32
    return temp_f


def convert_far_to_cel(temp_f):
    temp_c = (float(temp_f) -32) * 5/9
    return temp_c

temp_c = input("Enter a temeprature in degrees C: ")
temp_f = convert_cel_to_far(temp_c)

print(f"{float(temp_c):.2f} degrees C = {temp_f:.2f} degrees F")


temp_f = input("Enter a temeprature in degrees F: ")
temp_c = convert_far_to_cel(temp_f)

print(f"{float(temp_f):.2f} degrees F = {temp_c:.2f} degrees C")
