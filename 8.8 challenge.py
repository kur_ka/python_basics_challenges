# 8.8 Challenge : Simulate a Coin Toss Experiment

import random

def coin_flip():
    if random.randint(0,1) == 0:
        return "heads"
    else:
        return "tails"

def coin_toss_experiment():
    first_flip = coin_flip()
    flip_count = 1

    while coin_flip() == first_flip:
        flip_count += 1

    flip_count += 1 # Adding extra one for the last flip
        
    return flip_count

total_flips = 0
num_trials = 10000

for i in range(num_trials):    
    total_flips = total_flips + coin_toss_experiment()

avg = total_flips/num_trials

print(f"The result of experiment: \nThe average number of flips per {num_trials} trails is {avg}.")
    






            
        


        
    
    


