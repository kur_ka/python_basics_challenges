# 8.4 Challenge : Find the Factors of a Number

num = input("Give a positive integer: ")

if num.isdigit() and int(num)>0:
    for i in range(1,(int(num)+1)):
        modulo = int(num) % i
        if modulo == 0:
            print(f"{i} is a factor of {num}")
elif num[1:len(num)].isdigit()and int(num)<=0:
    print("Given number is not positive integer.") 
else:
    print("This is not a number.")
    


