# 9.5 challenge : Wax Poetic

import random

def list_random_choice(n, single_list):
    """ The function choose randomly from n different elements."""
    while True:
        random_list = [random.choice(single_list)for i in range(n)]
        if len(random_list) == len(set(random_list)):      
            break
    return random_list

noun = ["fossil",
    "horse",
    "aardvark",
    "judge",
    "chef",
    "mango",
    "extrovert",
    "gorilla",
]
verb = [
    "kicks",
    "jingles",
    "bounces",
    "slurps",
    "meows",
    "explodes",
    "curdles",
]
adjective = [
    "furry",
    "balding",
    "incredulous",
    "fragrant",
    "exuberant",
    "glistening",
]
preposition = [
    "against",
    "after",
    "into",
    "beneath",
    "upon",
    "for",
    "in",
    "like",
    "over",
    "within",
]
adverb = [
    "curiously",
    "extravagantly",
    "tantalizingly",
    "furiously",
    "sensuously",
]

noun = list_random_choice(10, noun)
verb = list_random_choice(3, verb)
adj = list_random_choice(3, adjective)
prep = list_random_choice(2, preposition)
adverb = list_random_choice(1, adverb)

if adj[0][0] in ["a", "e", "i", "o", "u"]:
    article = "An"
else:
    article = "A"

print(f"{article} {adj[0]} {noun[0]}")
print(f"{article} {adj[0]} {noun[0]} {verb[0]} {prep[0]} the {adj[1]} {noun[1]}")   
print(f"{adverb[0]}, the {noun[0]} {verb[1]}") 
print(f"the {noun[1]} {verb[2]} {prep[1]} a {adj[2]} {noun[2]}.")



            
        


        
    
    


