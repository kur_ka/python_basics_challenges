# 10.4 Challenge: Model a Farm

class Animal:
    feeding = 0

    def __init__(self, name, color):
        self.name = name
        self.color = color

    def feed(self):
        return f"{self.name} says yummy!"
        if feeding >= 2:
            return "Oh no... too much!"

    def speak(self, sound):
        return f"{self.name} says {sound}!"

    def poop(self):
        return f"OMG {self.name} did something very smelly!"

    def run(self, direction):
        self.direction = direction
        if direction == "left":
            return f"Good boy {self.name} this is the right way."
        elif direction == "right":
            return f"Oh no {self.name} you are running straight onto the road! STOP!"
        

class Cow(Animal):
    milk_count = 0
    
    def speak(self, sound = "Muu"):
        return super().speak(sound)

    def milk(self):
        self.milk_count += 1
        if self.milk_count == 1:
            return "Just a little bit more..."
        elif self.milk_count == 2:
            return "Oh there is already full bottle of milk"

class Chicken(Animal):
    eggs = 0
    
    def speak(self, sound = "kokoko"):
        return super().speak(sound)

    def egg_collecting(self):
        self.eggs += 5
        if self.eggs == 5:
            return f"Ou look what I found! 5 beaufifull eggs."
        elif self.eggs == 10:
            return f"And 5 more, now we can have a proper breakfest!"

class Horse(Animal):
    def speak(self, sound = "Ihaa !"):
        return sound

    def grooming(self, place):
        self.place = place
 
        if place == "back":
            return f"{self.name} likes it very much..."
        elif place == "tail":
            return f"No no no... please stop he doesn't like touching his tail!"
        else:            
            return self.speak()


horse = Horse("Storm", "brown")
chicken_1 = Chicken("Curry", "yellow")
chicken_2 = Chicken("Snowy", "white")
cow = Cow("Bella", "black-white")

print("Welcome to our family farm!")
print("Let me show you how a morning routine looks like in our family.")
print("The day begin with a trip to the barn.")

#Intruducing horse
print(f"This is our horse {horse.name}.")
print(horse.speak())
print(f"{horse.name} likes grooming. Would you like to try?")
print(horse.grooming("nose"))
print(horse.grooming("back"))
print(horse.grooming("tail"))
print(f"{horse.name} got little angry... Have some food {horse.name}")
print(horse.feed())
print(horse.feed())
print(horse.poop())

#Introducing chickens
print(f"Let me introduce you our chickens {chicken_1.name} and {chicken_2.name}.")
print(chicken_1.speak())
print(chicken_2.speak())
print("Good chickens, have some seeds.")
print(chicken_1.feed())
print("Every morning we need to collect eggs...")
print(chicken_1.egg_collecting())
print(chicken_1.egg_collecting())

#Introducing cow
print(cow.speak())
print(f"Sorry I almost forget. This is our cow {cow.name}.")
print(f"We have to milk {cow.name} every morning. Let's do this together")
print(cow.milk())
print(cow.milk())

print("After the morning routine we let animals out.")
print(chicken_1.run("left"))
print(cow.run("right"))

    

    
 


            




    

            
        


        
    
    


