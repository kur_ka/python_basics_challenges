# 5.3 Challenge: Perform Calculations on User Input

num1 = input("Enter a base: ")
num2 = input("Enter an exponent: ")

result = float(num1) ** float(num2)

print(num1 + " to the power of " + num2 + " = " + str(result))
