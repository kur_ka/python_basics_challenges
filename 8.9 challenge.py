# 8.9 challenge : Simulate an Election

# Probability of winning for candidate A shown below:
import random

prob_region_1 = 0.87
prob_region_2 = 0.65
prob_region_3 = 0.17

def winning_in_region(prob_region):
    if random.random() < prob_region:
        return "candidate A"
    else:
        return "candidate B"

def trial_election():
    win_1 = winning_in_region(prob_region_1)
    win_2 = winning_in_region(prob_region_2)
    win_3 = winning_in_region(prob_region_3)

    if win_1 == "candidate A" and win_2 == "candidate A": 
        return "candidate A"
    elif win_1 == "candidate A" and win_3 == "candidate A": 
        return "candidate A"
    elif win_2 == "candidate A" and win_3 == "candidate A": 
        return "candidate A"
    else:
        return "candidate B"

candidate_A = 0
candidate_B = 0

number_of_simulations = 10000

for trial in range(number_of_simulations):
    winner = trial_election()
    if winner == "candidate A":
        candidate_A += 1
    else:
        candidate_B += 1

ratio = candidate_A/number_of_simulations

print(f" Candidate A won in this simulation with ratio {ratio:.2f}")
    
    


    






            
        


        
    
    


