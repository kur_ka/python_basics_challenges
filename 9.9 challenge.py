# 9.9 challenge : Cats with Hats

# I'm creating list of cats without hats,
# to make thing easier I'm adding one cat extra, and during program cat
# from list with index [0] will be ignored (value 0 will not be altered).

cats_with_hats = [0 for n in range(101)] # value 0 means 'no hats'

for n in range(1,101):
    for k in range (1,101):
        if n % k == 0 and cats_with_hats[n] == 0:
            cats_with_hats[n] = 1 # value 1 means 'with hat'
        elif n % k == 0 and cats_with_hats[n] == 1:
            cats_with_hats[n] = 0

print("After 100 rounds the below cats have a hat:")

cats_after_100 = []
for n in range(1,101):
    if cats_with_hats[n] == 1:
        cats_after_100.append(n)

print(cats_after_100)
 


            




    

            
        


        
    
    


