# 9.4 challenge : List of list

def enrollment_stats(list_of_lists):
    student_enrollments = [value[1] for value in list_of_lists]   
    tuition_fees = [value[2] for value in list_of_lists] 
    return [student_enrollments, tuition_fees]

def mean(single_list):
    return sum(single_list)/len(single_list)

def median(single_list):
    single_list.sort()
    if len(single_list)% 2 == 0:
        mid_index_1 = int(len(single_list)/2)
        mid_index_2 = int(len(single_list)/2 + 1)
        median = (single_list[mid_index_1]+ single_list[mid_index_2])/ 2

    else:
        mid_index = int(len(single_list)/2)
        median = single_list[mid_index]
    return median
  

universities = [
    ["California Institute of Technology", 2175, 37704],
    ["Harvard", 19627, 39849],
    ["Massachusetts Institute of Technology", 10566, 40732],
    ["Princeton", 7802, 37000],
    ["Rice", 5879, 35551],
    ["Stanford", 19535, 40569],
    ["Yale", 11701, 40500],
]

student_enrollments, tution_fees = enrollment_stats(universities)
total_students = sum(student_enrollments)
total_tuition = sum(tution_fees)

student_mean = mean(student_enrollments)
student_median = median(student_enrollments)

tution_mean = mean(tution_fees)
tution_median = median(tution_fees)

print("*************************")
print(f"Total sudents: {total_students:,.0f}")
print(f"Total tuition: $ {total_tuition}")
print()
print(f"Student mean: {student_mean:,.2f}")
print(f"Student median: {student_median:,.0f}")
print()
print(f"Tution mean: $ {tution_mean:,.2f}")
print(f"Tution median: $ {tution_median:,.0f}")
print("*************************")

    






            
        


        
    
    


